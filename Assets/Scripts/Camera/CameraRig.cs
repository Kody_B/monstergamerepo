﻿using UnityEngine;
using System.Collections;

public class CameraRig : MonoBehaviour {

	public float m_DampTime = 0.2f;
	private Transform m_playerPos;

	private Vector3 m_DesiredPosition;
	private Vector3 m_MoveVelocity;

	void Start () 
	{
		m_playerPos = GameObject.FindGameObjectWithTag("Player").transform;
	}

	void Update()
	{
		m_DesiredPosition = m_playerPos.transform.position;
	}

	void FixedUpdate()
	{
		Move();
	}

	void Move()
	{
		transform.position = Vector3.SmoothDamp(transform.position, m_DesiredPosition, ref m_MoveVelocity, m_DampTime);
	}
}
