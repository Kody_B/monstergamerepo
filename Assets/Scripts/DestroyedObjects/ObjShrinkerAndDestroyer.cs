﻿using UnityEngine;
using System.Collections;

public class ObjShrinkerAndDestroyer : MonoBehaviour {

	[HideInInspector]
	public float secondsToDie;

	private bool canShrink;

	void Start () 
	{
		StartCoroutine(StartShrink());
	}

	void Update () 
	{
		if(canShrink)
		{
			transform.localScale -= new Vector3(0.2f * Time.deltaTime, 0.2f * Time.deltaTime, 0.2f * Time.deltaTime);

			if(transform.localScale.x <= 0f)
			{
				Destroy(gameObject);
			}
		}
	}

	IEnumerator StartShrink()
	{
		yield return new WaitForSeconds(secondsToDie);
		//GetComponent<Rigidbody>().isKinematic = true;
		//GetComponent<Collider>().enabled = false;
		canShrink = true;
	}
}
