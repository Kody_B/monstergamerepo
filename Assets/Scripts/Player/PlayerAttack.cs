﻿using UnityEngine;
using System.Collections;

public class PlayerAttack : MonoBehaviour {

	public float m_ImpactForce;
	public float m_ImpactRadius;

	public float m_RightAttackLength;
	public float m_LeftAttackLength;
	public float m_TopAttackLength;

	[SerializeField] GameObject m_RightCanHitCollider;
	[SerializeField] Transform m_RightHitPosition;

	[SerializeField] GameObject m_LeftCanHitCollider;
	[SerializeField] Transform m_LeftHitPosition;

	private bool m_IsAttacking;

	private int m_RightArmSwingHash = Animator.StringToHash("RightArmSwing");
	private int m_LeftArmSwingHash = Animator.StringToHash("LeftArmSwing");
	private int m_TopDownSwingHash = Animator.StringToHash("TopDownSwing");

	private Animator anim;
	private PlayerMovement playerMovement;

	void Awake()
	{
		playerMovement = GetComponent<PlayerMovement>();
		anim = GetComponentInChildren<Animator>();
	}

	void Update()
	{
		//right arm attack
		if(Input.GetButtonDown("Fire2") && !m_IsAttacking && !playerMovement.m_IsWalking)
		{
			StartCoroutine(Attack(m_RightCanHitCollider, m_RightArmSwingHash, m_RightAttackLength));
		}
		//left arm attack
		else if(Input.GetButtonDown("Fire1") && !m_IsAttacking && !playerMovement.m_IsWalking)
		{
			StartCoroutine(Attack(m_LeftCanHitCollider, m_LeftArmSwingHash, m_LeftAttackLength));
		}
		//right arm top down attack
		else if(Input.GetButtonDown("Jump") && !m_IsAttacking && !playerMovement.m_IsWalking)
		{
			StartCoroutine(Attack(m_RightCanHitCollider, m_TopDownSwingHash, m_TopAttackLength));
		}
	}

	IEnumerator Attack(GameObject attackCol, int animHash, float attackLength)
	{
		m_IsAttacking = true;
		attackCol.SetActive(true);

		anim.SetTrigger(animHash);

		yield return new WaitForSeconds(attackLength);

		m_IsAttacking = false;
		attackCol.SetActive(false);
	}

	//current melee logic
	void OnTriggerEnter(Collider other)
	{
		if(other.GetComponent<FracturedChunk>() == true)
		{
			RaycastHit hit;
			if(Physics.Linecast(m_RightHitPosition.position, other.transform.position, out hit))
			{
				FracturedChunk chunk = other.GetComponent<FracturedChunk>();
				if(chunk == null)
				{
					return;
				}

				chunk.Impact(hit.point, m_ImpactForce, m_ImpactRadius, false);
			}
		}
	}
}
