﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float m_Speed;
	public float m_TurnSpeed;

	private Transform m_Cam;
	private Vector3 m_CamForward;

	private Vector3 m_Move;

	private float m_TurnAmount;

	private int m_IsWalkingHash = Animator.StringToHash("isWalking");
	[HideInInspector] public bool m_IsWalking;

	private Rigidbody m_rb;
	private Animator anim;

	void Awake()
	{

		m_rb = GetComponent<Rigidbody>();
		anim = GetComponentInChildren<Animator>();

	}

	void FixedUpdate()
	{
		float h = Input.GetAxisRaw("Horizontal");
		float v = Input.GetAxisRaw("Vertical");

		if(Mathf.Abs(h) > 0 || Mathf.Abs(v) > 0)
		{
			m_IsWalking = true;
		}
		else
		{
			m_IsWalking = false;
		}
			
		Move(h, v);

		UpdateAniamtion();

	}

	void Move(float h, float v)
	{

		m_Move.Set(h, 0.0f, v);

		m_Move = m_Move.normalized * m_Speed * Time.deltaTime;

		m_rb.MovePosition(transform.position + m_Move);

		if(m_Move != Vector3.zero)
			Turn();

	}

	void Turn()
	{
		Quaternion newRotation = Quaternion.LookRotation(m_Move);

		m_rb.rotation = Quaternion.RotateTowards(m_rb.rotation, newRotation, m_TurnSpeed * Time.deltaTime);

	}

	void UpdateAniamtion()
	{
		anim.SetBool(m_IsWalkingHash, m_IsWalking);
	}
}
